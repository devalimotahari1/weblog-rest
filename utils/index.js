const getWebUrl = (req) => {
    return req.protocol + '://' + req.get('host');
}

module.exports = {
    getWebUrl,
}