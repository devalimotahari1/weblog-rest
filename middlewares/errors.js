exports.errorHandler = (error, req, res, next) => {
    console.log("Error occurred: ", error, "\n", error.message, "\n", error.data);
    const status = error.statusCode || 500;
    const message = error.message;
    const data = error.data;
    res.status(status).json({message, data});
};
